Name:           jakarta-activation
Version:        1.2.2
Release:        1
Summary:        Jakarta Activation Specification and Implementation
License:        BSD
URL:            https://eclipse-ee4j.github.io/jaf/
BuildArch:      noarch

Source0:        https://github.com/eclipse-ee4j/jaf/archive/%{version}/jaf-%{version}.tar.gz

BuildRequires:  java-11-openjdk-devel
BuildRequires:  maven-local
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-dependency-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-enforcer-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-source-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)

Requires:       java-11-openjdk

%description
Jakarta Activation lets you take advantage of standard services to:
determine the type of an arbitrary piece of data; encapsulate access to
it; discover the operations available on it; and instantiate the
appropriate bean to perform the operation(s).

%package javadoc
Summary:        Javadoc for %{name}

%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n jaf-%{version}

%pom_remove_parent
%pom_disable_module demo

%pom_remove_plugin :directory-maven-plugin
sed -i 's/${main.basedir}/${basedir}/' pom.xml

%pom_remove_plugin :maven-javadoc-plugin activation

%pom_remove_plugin :osgiversion-maven-plugin
sed -i "s/\${activation.osgiversion}/%{version}/g" activation/pom.xml

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"

%if 0%{?rhel}
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk
%endif

%mvn_build -j

%install
%mvn_install

%files -f .mfiles
%doc README.md
%license LICENSE.md NOTICE.md

%files javadoc
%license LICENSE.md NOTICE.md

%changelog
* Thu Dec 23 2021 wangkai <wangkai385@huawei.com> - 1.2.2-1
- Initial package

